from mongoengine import connect
from settings import *

from tasks import gather_forecast

connect(MONGO_DATABASE)

report = gather_forecast()

from pprint import PrettyPrinter
p = PrettyPrinter(indent=2)
p.pprint(report)
