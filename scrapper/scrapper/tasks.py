from settings import *
from datetime import timedelta
from celery import Celery
from mongoengine import connect
from documents import SpotForecast
from reports import ProcessingReport
import redis

connect(MONGO_DATABASE)

app = Celery('tasks', broker=BROKER_URL)
app.conf.update(
    CELERYBEAT_SCHEDULE={
        'gather_forecast_every_minute': {
            'task': 'tasks.gather_forecast',
            'schedule': timedelta(minutes=1),
        },
    }
)


@app.task
def gather_forecast():
    a = SpotForecast.query_last(WINDGURU_SPOT)
    a = ProcessingReport.prettify(a.report())
    r = redis.Redis()
    r.set('latest_forecast', a)
    return a
