from dateutil import parser

import zerorpc
c = zerorpc.Client()
c.connect("tcp://127.0.0.1:4242")

class ProcessingReport(object):    
    @classmethod
    def prettify(cls, report):
        
        r = [
            {
                'temperature': {
                    'color': ProcessingReport.temperature_to_color(item['temperature']),
                    'degrees': "%s" % int(item['temperature']),
                },
                'timestamp': ProcessingReport.pretty_timestamp(item['timestamp']),
                'precipitations': item['precipitations'],
                'wind': {
                    'direction': item['wind_direction'],
                    'speed': item['wind_speed'],
                },
                'delta': item['delta'],
            }
            for item in report
        ]
        return r

    @classmethod
    def temperature_to_color(cls, t):
        color = c.temperature_to_color(t)
        return color

    @classmethod
    def pretty_timestamp(cls, ts):
        WEEKDAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        d = parser.parse(ts)
        return '%s %02d:%02d' % (WEEKDAYS[d.weekday()], d.hour, d.minute)