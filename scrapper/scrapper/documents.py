import calendar
import urllib
import math
import simplejson as json

from pyquery import PyQuery as pq

from datetime import datetime as dt
from datetime import timedelta as td

from dateutil import parser
from mongoengine import *

class DTForecast(EmbeddedDocument):
    index = DateTimeField()
    temperature = FloatField()
    wind_speed = FloatField()
    wind_direction = IntField()
    precipitations = FloatField()

    def __unicode__(self):
        return '(%s) T:%s P:%s W:%s-%s' % (
            self.index,
            self.temperature,
            self.precipitations,
            self.wind_speed,
            self.wind_direction
        )

class SpotForecast(Document):
    daylies = ListField(EmbeddedDocumentField(DTForecast))
    spot = IntField()
    index = DateTimeField()
    next_forecast = DateTimeField()
    sunrise = DateTimeField()
    sunset = DateTimeField()
    latitude = FloatField()
    longitude = FloatField()
    altitude = FloatField()
    tzid = StringField()

    @classmethod
    def ssfromstring(self, day, ss):
        ss = ss.split(':')
        ss = [int(s) for s in ss]
        ss = dt(
            year=day.year,
            month=day.month,
            day=day.day,
            hour=ss[0],
            minute=ss[1]
        )
        return ss

    @classmethod
    def query_last(cls, spot_id):
        data = SpotForecast.WGData(spot_id)
        forecast = data["fcst"][data["id_model"]]
        index = parser.parse(forecast["update_last"])
        s = SpotForecast.objects.filter(spot=spot_id, index=index)
        if not s.count():
            s = SpotForecast()
            s.spot = spot_id
            s.index = index
            s.next_forecast = parser.parse(forecast["update_next"])
            s.sunrise = SpotForecast.ssfromstring(s.index, data['sunrise'])
            s.sunset = SpotForecast.ssfromstring(s.index, data['sunset'])
            s.latitude = float(data['lat'])
            s.longitude = float(data['lon'])
            s.altitude = float(data['alt'])
            s.tzid = data['tzid']
            # Parsing dailys
            for i in range(forecast['TMP'].__len__()):
                d = DTForecast()
                d.index = dt(
                    year=s.index.year,
                    month=s.index.month,
                    day=int(forecast['hr_d'][i]),
                    hour=int(forecast['hr_h'][i]),
                )
                if s.index.day > d.index.day:
                    days = calendar.monthrange(
                        s.index.year,
                        s.index.month,
                    )[1]
                    d.index = d.index + td(days)
                d.temperature = float(forecast['TMPE'][i])
                d.precipitations = float(forecast['PCPT'][i])
                d.wind_direction = int(forecast['WINDDIR'][i])
                d.wind_speed = float(forecast['WINDSPD'][i])
                s.daylies.append(d)
            s.save()
        else:
            s = s[0]
        return s

    @classmethod
    def WGData(cls, spot_id):
        URL = 'http://www.windguru.cz/en/index.php?sc=%s' % spot_id
        d = pq(
            url=URL,
            opener=lambda url: urllib.urlopen(url).read()
        )
        j = d("#div_wgfcst1 script").text()
        limits = [
            j.index("var wg_fcst_tab_data_1 = ") + "var wg_fcst_tab_data_1 = ".__len__(),
            j.index(";")
        ]
        j = j[limits[0]:limits[1]]
        return json.loads(j)

    def report(self):
        now = dt.now()
        hours = [i for i in range(36) if i > 0 and not i % 3]
        
        now = dt(
            year=now.year,
            month=now.month,
            day = now.day,
            hour=(now.hour - now.hour % 3),
        )
        report = []
        for d in self.daylies:
            if d.index:
                delta = d.index - now
                if (d.index - dt.now()) >= td(hours=0):
                    h = delta.days * 24 + math.floor(delta.seconds / 3600)
                    if h in hours or (delta.days > 1 and d.index.hour in [12, 18]):
                        i = delta.days
                        ts = d.index
                        del d.index
                        o = json.loads(d.to_json())
                        o['timestamp'] = '%s' % ts
                        o['delta'] = h
                        report.append(o)
        return report

    def __unicode__(self):
        return u'%s|%s at (%s,%s) %s altitude: %s - NF: %s' % (
            self.spot, self.tzid,
            self.latitude, self.longitude, self.altitude,
            self.index,
            self.next_forecast
        )
