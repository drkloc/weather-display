from nanpy import *
from time import sleep
from settings import *

connection = SerialManager(ARDUINO_SERIAL_PORT)
a = ArduinoApi(connection=connection)


a.pinMode(12, Arduino.OUTPUT)
a.pinMode(11, Arduino.OUTPUT)
a.pinMode(10, Arduino.OUTPUT)

def turn(pin):
	for i in range(10,13):
		if i != pin:
			a.digitalWrite(i, a.LOW)
	if(pin):
		a.digitalWrite(pin, a.HIGH)

from mongoengine import connect
from documents import SpotForecast

connect(MONGO_DATABASE)

s = SpotForecast.objects.all()

if s.count():
	s = s[0]
	for d in s.daylies:
		print 'Temperature on %s will be %s' % (d.index, d.temperature)
		if d.temperature < 2:
			turn(10)
		elif d.temperature < 9:
			turn(11)
		else:
			turn(12)
		sleep(0.2)
turn(0)