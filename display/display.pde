import org.json.*;
import de.voidplus.redis.*;

Redis redis;

int rows = 3;
int cols = 5;
int squares = rows * cols;
int scale = 2;
int screen_w = 1920;
int screen_h = 1080;

PFont degrees_font;
PFont day_n_hour_font;

int degrees_font_size = 250;
int day_n_hour_font_size = 80;

JSON latest_forecast;
Cell[] grid;

void setup() {
  redis = new Redis(this, "127.0.0.1", 6379);
  size(screen_w/scale, screen_h/scale);
  colorMode(HSB, 1, 1, 1);
  degrees_font = loadFont("GillSans-Light-250.vlw");
  day_n_hour_font = loadFont("GillSans-Light-80.vlw");
  fetchLatestForecast();
  setupRectangles();
}

void draw() {
  background(255);
  for (int i = 0; i < squares ; i ++ ) {
    grid[i].display();
  }
  //fetchLatestForecast();
}

void fetchLatestForecast() {
  String redis_forecast = redis.get("latest_forecast");
  latest_forecast = JSON.parse(redis_forecast);
}

void setupRectangles() {
  grid = new Cell[squares];
  int w = screen_w / scale / cols;
  int h = screen_h / scale / rows;
  for(int i = 0; i < squares; i++) {
    int j = i / cols;
    int x = (i - j * squares / rows) * w;
    int y = j * h;
    grid[i] = new Cell(i, x, y, w, h);
  }
}


class Cell {
  int k; // index
  float x,y;   // x,y location
  float w,h;   // width and height
  float angle; // angle for oscillating brightness

  Cell(int tempK, float tempX, float tempY, float tempW, float tempH) {
    k = tempK;
    x = tempX;
    y = tempY;
    w = tempW;
    h = tempH;
  }
  
  void display() {
    noStroke();

    // Get item for index
    JSON item = latest_forecast.getJSON(k);
    
    // Get temperature object    
    JSON temperature = item.getObject("temperature");

    // Create and Color Rectangle from color property
    JSON colr = temperature.getObject("color");
    float hue = colr.getFloat("hue");
    float saturation = colr.getFloat("saturation");
    float luminance = colr.getFloat("luminance");
    color c = color(hue, saturation, luminance);
    fill(c);
    rect(x,y,w,h);
    
    // Create degree text    
    String degrees = temperature.getString("degrees");
    textFont(degrees_font, degrees_font_size / scale);
    fill(1, 0, 1);
    textAlign(RIGHT, TOP);
    text(degrees, x + w - 20 / scale, y + 40 / scale);
    
    // Create day n' hour text
    String day_n_hour = item.getString("timestamp");
    textFont(day_n_hour_font, day_n_hour_font_size / scale);
    fill(1, 0, 1);
    textAlign(RIGHT, BOTTOM);
    text(day_n_hour, x + w - 20 / scale, y + h);
  }
}

public class Rain
{
  PVector position, pposition, speed;
  float col;
  
  float x,y;   // x,y limits
  float w,h;   // width and height limits
  
  public Rain(float tempX, float tempY, float tempW, float tempH)
  {
    k = tempK
    x = tempX;
    y = tempY;
    w = tempW;
    h = tempH;
    
    position = new PVector(x+random(0,w), y);
    pposition = position;
    speed = new PVector(0,0);
    col = random(30,100);
  }
   
  void draw()
  {
    stroke(100, col);
    strokeWeight(2);
    line(position.x,position.y,pposition.x,pposition.y);
    //ellipse(position.x,position.y,5,5);
  }
   
  void calculate()
  {
    pposition = new PVector(position.x,position.y);
    gravity();
 
  }
   
  void gravity()
  {
    speed.y += .2;
    speed.x += .01;
    position.add(speed);
  }
}

