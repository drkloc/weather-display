var chroma = require("chroma-js");
var zerorpc = require("zerorpc");

var server = new zerorpc.Server({
    temperature_to_color: function(temperature, reply) {
        var colors = [];
        var p = 0;
        var UNDER_ZERO = -20;
        var OVER_ZERO = 20;

        if(temperature < 0) {
            colors = ['#4e47d3', '#3f80eb', '#82e9e5', 'white'];
            if (temperature > UNDER_ZERO) {
                p = (temperature - UNDER_ZERO) / (-UNDER_ZERO);
            }
        }
        else {
            colors = ['#f04b66', '#f08d4b', '#9ae982', 'white'];
            if (temperature < OVER_ZERO) {
                p = (OVER_ZERO - temperature) / OVER_ZERO;
            }
        }
        var interpolator = chroma.interpolate.bezier(colors);
        var scale = chroma.scale(interpolator).mode('lab').correctLightness(true);
        var color = scale(p).hsv();
        color = {
            'hue': color[0] / 360,
            'saturation': color[1],
            'luminance': color[2]
        };
        reply(null, color);
    }
});

server.bind("tcp://0.0.0.0:4242");